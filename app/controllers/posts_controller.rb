class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.all
    layout_rendering = (params[:skip_layout] == "yes") ? false : true
    render "index", layout: layout_rendering
  end

  def show
  end

  def new
    @post = Post.new
  end

  def edit
  end

  def create
    @post = Post.new(post_params)

    respond_to do |format|
      if @post.save
        format.html do
          redirect_to posts_url
        end
        format.turbo_stream do
          render turbo_stream: turbo_stream.append('list',  
                                                      partial: "posts/line",
                                                      locals: { post: @post, notice: "Post created!" })
        end

      else
        format.html do
          render "edit", locals: { post: @post }
        end
        format.turbo_stream do
          render turbo_stream: turbo_stream.update('modal',
                                                      partial: "posts/modal_form",
                                                      locals: { post: @post }), status: 400
        end

      end
    end
  end

  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html do
          redirect_to posts_url
        end
        format.turbo_stream do
          render turbo_stream: turbo_stream.replace(@post.tag_id,
                                                    partial: "posts/line",
                                                    locals: { post: @post })
        end

      else
        format.html do
          render "edit", locals: { post: @post }
        end
        format.turbo_stream do
          render turbo_stream: turbo_stream.update('modal',
                                                    partial: "posts/modal_form",
                                                    locals: { post: @post }), status: 400
        end

      end
    end
  end

  def destroy
    @post.destroy
    respond_to do |format|
      format.html do
        redirect_to posts_url
      end
      format.turbo_stream do
        render turbo_stream: turbo_stream.replace(@post.tag_id,
                                                  partial: "posts/line",
                                                  locals: { post: @post, deleted: "yes" })
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_post
    @post = Post.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def post_params
    params.require(:post).permit(:title, :body)
  end
end
