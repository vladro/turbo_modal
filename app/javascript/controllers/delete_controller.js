import { Controller } from "stimulus"

export default class extends Controller {
  static values = { confirmMessage: String };

  confirm(event) {
    if (!(window.confirm(this.confirmMessageValue))) {
      event.preventDefault();
      event.stopImmediatePropagation();
    };
  };

  exit() {
    const event = new CustomEvent("modal:close", { bubbles: true, cancelable: true });
    document.dispatchEvent(event);
  }

}