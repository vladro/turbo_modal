import { Controller } from "stimulus";

export default class extends Controller {
  static values = {
    matchLinks: String
  }

  connect() {
    
    window.addEventListener('popstate', (e) => {
      var path_name = window.location.pathname;
      if (path_name.match(new RegExp(this.matchLinksValue))) {
        var link = document.querySelectorAll("a[href='" + path_name + "']");
        link[0].click();
      } else {
        const event = new CustomEvent("modal:close", { bubbles: true, cancelable: true });
        document.dispatchEvent(event);
      }
    });

  }
}